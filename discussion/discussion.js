// >>> SECTION: Comparison Query Operators [ <, >, =, != ]

// $gt / $gte operator
db.users.find({age: {$gt: 50}});
db.users.find({age: {$gte: 50}});

// $lt / $lte operator
db.users.find({age: {$lt: 50}});
db.users.find({age: {$lte: 50}});

// $ne operator
db.users.find({age: {$ne: 82}});

// $in operator
db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
db.users.find({lastName: {$in: ["HTML", "React"]}});

// >>> SECTION: Logical Query Operators [||, &&]
// $or
db.users.find({$or: [{firstName: "Neil"}, {age: 21}]});
db.users.find({$or: [{firstName: "Neil"}, {age: 21}]});

// $or + $gt
db.users.find({$or:[{firstName: "Neil"}, {age: {$gt: 21}}]});

// $and
db.users.find({$and:[{age: {$ne: 76}}, {age: {$ne: 76}}]});

// >>> SECTION: Field Projection
db.users.find(
  {firstName: "Jane"},
  {
    firstName: 1,
    lastName: 2,
    contact: 1
  }
);

db.users.find(
  {firstName: "Jane"},
  {
    firstName: 1,
    lastName: 2,
    contact: 1
  }
);

db.users.find(
  {firstName: "Jane"},
  {
    contact: 0,
    department: 0
  }
);

// miniactivity
db.users.find(
  {firstName: "Jane"},
  {
    id: 0,
    firstName: 1,
    lastName: 1,
    contact: 1
  }
);

// Suppressing of the ID
db.users.find(
  {firstName: "Jane"},
  {
    firstName: 1,
    lastName: 1,
    contact: 1,
    _id: 0
  }
);

db.users.find(
  {firstName: "Jane"},
  {
    firstName: 1,
    lastName: 1,
    contact: 1,
    _id: 0
  }
);

// one way
db.users.find(
    {firstName: "Jane"},
    {
      firstName: 1,
      lastName: 1,
      contact: {
        phone: 1
      }
  });

  // another way - use json "contact.phone"
  db.users.find(
	{ firstName: "Jane" },
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}
);

// Suppressing Specific Fields in Embedded Documents
db.users.find(
	{ firstName: "Jane" },
	{
		"contact.phone": 0
	}
)

// Project Specific Array Elements in the Returned Array

// $slice operator - non-mutator (return new array)
db.users.find(
  {
    "nameArr":{nameA: "juan"}
  },
  {
  "nameArr":{$slice: 1}
  }
);

// >>> SECTION: Evaluation Query Operators
// $regex operator (reg expression - string)

// Case-sensitive query
db.users.find({
  firstName: {$regex: "j"}
});

// Case-insensitive, with the use of "$i
db.users.find({
  firstName: {$regex: "j", 
  $options: "$i"
}
});